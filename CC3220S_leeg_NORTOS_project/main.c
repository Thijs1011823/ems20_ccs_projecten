#include <stdint.h>
#include <stddef.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>

/* Driver configuration */
#include "ti_drivers_config.h"

/*
 *  ============= mainThread ===============
 *
 *  Dit is de 'main' functie van het project
 *  Debugger is ingesteld om hier te te pauzeren
 *  i.p.v. bij de originele main-functie
 *
 *  Tips:
 *  1. Gebruik 'Board.html' uit het project om
 *     de pinnamen te zien van de driver
 *
 */
void *mainThread(void *arg0)
{
    /*
     *  code komt hier
     *
     */
    GPIO_init();

    while(1)
    {
        GPIO_toggle(CONFIG_GPIO_LED_0);
    }
}
